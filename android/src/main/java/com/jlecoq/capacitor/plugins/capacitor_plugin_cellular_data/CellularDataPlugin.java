package com.jlecoq.capacitor.plugins.capacitor_plugin_cellular_data;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.content.Context;
import android.util.Log;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

@NativePlugin
public class CellularDataPlugin extends Plugin {

    private static final String USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_CODE = "1";
    private static final String USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_MESSAGE = "use cellular data for internet access failed.";

    @PluginMethod(returnType = PluginMethod.RETURN_NONE)
    public void useCellularDataForInternetAccess(final PluginCall call) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) {
            call.error(USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_MESSAGE,
                    USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_CODE,
                    null);
            return;
        }

        NetworkRequest req = new NetworkRequest
            .Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build();

        ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);

                if (connectivityManager.bindProcessToNetwork(network)) {
                    call.success();
                } else {
                    call.error(USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_MESSAGE,
                            USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_CODE,
                            null);
                }
            }

            @Override
            public void onUnavailable() {
                super.onUnavailable();
                call.error(USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_MESSAGE,
                        USE_CELLULAR_DATA_FOR_INTERNET_ACCESS_ERROR_CODE,
                        null);
            }
        };

        connectivityManager.requestNetwork(req, networkCallback);
    }
}
