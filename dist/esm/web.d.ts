import { WebPlugin } from '@capacitor/core';
import { CellularDataPluginPlugin } from './definitions';
export declare class CellularDataPluginWeb extends WebPlugin implements CellularDataPluginPlugin {
    constructor();
    useCellularDataForInternetAccess(): Promise<void>;
}
declare const CellularDataPlugin: CellularDataPluginWeb;
export { CellularDataPlugin };
