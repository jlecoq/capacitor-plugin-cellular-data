import { WebPlugin } from '@capacitor/core';
import { CellularDataPluginPlugin } from './definitions';

export class CellularDataPluginWeb extends WebPlugin implements CellularDataPluginPlugin {
  constructor() {
    super({
      name: 'CellularDataPlugin',
      platforms: ['web'],
    });
  }

  async useCellularDataForInternetAccess(): Promise<void> {
    throw new Error('This method does not support web platform.');
  }
}

const CellularDataPlugin = new CellularDataPluginWeb();

export { CellularDataPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(CellularDataPlugin);
