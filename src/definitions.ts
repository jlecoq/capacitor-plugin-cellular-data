declare module '@capacitor/core' {
    interface PluginRegistry {
        CellularDataPlugin: CellularDataPluginPlugin;
    }
}

export interface CellularDataPluginPlugin {
    useCellularDataForInternetAccess(): Promise<void>;
}
